<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

namespace App\Services;

class TestService {
   
    private $kurs = 60;
    
    public function convert ($rub) {
        return $rub/$this->kurs;
    }

}
