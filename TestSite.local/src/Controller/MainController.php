<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Services\TestService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
    * @Route("/main", name="main")
    */

    public function index(TestService $service)
    {          
        $vivod =  $service->convert(1000);

        return $this->render('main/index.html.twig', [
            'key' => $vivod,
            'title' => 'Заголовок',
            ]);

    }          
    
    
    

}
