<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class LuckyController
{
    public function number()
    {
        $generator = \Nubs\RandomNameGenerator\All::create();

        return new Response(
            '<html><body>Lucky number: '.$generator->getName().
            '</body></html>'
        );
    }
    

}

